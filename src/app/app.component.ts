import { Component, OnInit } from '@angular/core';
import { FavouriteChangedEventrArgs } from './shared/components/favourite/favourite.component';
import { NavigationRoute } from './shared/components/navbar/navbar.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Angular App';

  private routes: NavigationRoute[];
  private defaultRoute: NavigationRoute;

  ngOnInit() {
    this.routes = [
      { url: '/followers', displayName: 'Followers', queryParams: { page: 1, order: 'newest' } },
      { url: '/posts', displayName: 'Posts' },
      { url: '/blogs', displayName: 'Blogs' },
      { url: '/restaurants', displayName: 'Restaurants' },
      { url: '/login', displayName: 'Login' },
      { url: '/register', displayName: 'Register' },
      { url: '/profile', displayName: 'Profile' }
    ];
    this.defaultRoute = this.routes[0];
  }

  onFavouriteChanged(args: FavouriteChangedEventrArgs) {
    console.info(`Is Favourite: ${args.newValue}`);
  }
}
