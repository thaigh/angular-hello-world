import { Auth0RoutingModule } from './auth0-routing.module';
import { Auth0Service } from './services/auth0.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthCallbackComponent } from './pages/auth-callback/auth-callback.component';
import { Auth0ProfileComponent } from './components/auth0-profile/auth0-profile.component';
import { NgxJsonViewerModule } from 'ngx-json-viewer';

@NgModule({
  declarations: [AuthCallbackComponent, Auth0ProfileComponent],
  imports: [
    CommonModule,
    Auth0RoutingModule,
    NgxJsonViewerModule
  ],
  providers: [
    Auth0Service
  ],
  exports: [
    Auth0ProfileComponent
  ]
})
export class Auth0Module { }
