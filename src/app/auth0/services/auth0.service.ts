import { LocalStorageService } from './../../shared/services/local-storage.service';
import { Injectable } from '@angular/core';

import { auth0Config } from '../auth0.config';
import * as auth0 from 'auth0-js';
import { Router } from '@angular/router';

@Injectable()
export class Auth0Service {

  private static readonly IdTokenStorageKey = 'id_token';
  private static readonly AccessTokenStorageKey = 'access_token';
  private static readonly UserProfileStorageKey = 'user_profile';
  private static readonly ExpiresAtTokenStorageKey = 'id_token_expires_at';

  private auth0 = new auth0.WebAuth(auth0Config);

  private get idToken(): string { return this._storage.get(Auth0Service.IdTokenStorageKey) || undefined; }
  private get accessToken(): string { return this._storage.get(Auth0Service.AccessTokenStorageKey) || undefined; }
  // tslint:disable-next-line:max-line-length
  private get userProfile(): auth0.Auth0UserProfile { return this._storage.getData<auth0.Auth0UserProfile>(Auth0Service.UserProfileStorageKey) || undefined; }
  private get expiresAt(): number { return this._storage.getData<number>(Auth0Service.ExpiresAtTokenStorageKey) || undefined; }

  private set idToken(idToken) { this._storage.set(Auth0Service.IdTokenStorageKey, idToken); }
  private set accessToken(accessToken) { this._storage.set(Auth0Service.AccessTokenStorageKey, accessToken); }
  private set userProfile(userProfile) { this._storage.setData(Auth0Service.UserProfileStorageKey, userProfile); }
  private set expiresAt(expiresAt) { this._storage.setData(Auth0Service.ExpiresAtTokenStorageKey, expiresAt); }

  constructor(
    private _router: Router,
    private _storage: LocalStorageService
  ) { }

  login() {
    this.auth0.authorize();
  }

  public handleAuthentication(): Promise<boolean> {

    return new Promise( (resolve, reject) => {

      if (!window.location.hash) {
        return resolve(this.isAuthenticated());
      }

      this.parseHash()
        .then( (authResult: auth0.Auth0DecodedHash) => {
          window.location.hash = '';
          this.localLogin(authResult);
          resolve(this.isAuthenticated());
        })
        .catch( (err: auth0.Auth0ParseHashError) => {
          reject(err);
        });
    });
  }

  public parseHash() {
    return new Promise( (resolve, reject) => {
      this.auth0.parseHash( (err: auth0.Auth0ParseHashError, authResult: auth0.Auth0DecodedHash) => {

        if (authResult && authResult.accessToken && authResult.idToken) {
          resolve(authResult);
        } else {
          reject(err);
        }

      });
    });
  }

  private localLogin(authResult: auth0.Auth0DecodedHash): void {
    // Set the time that the Access Token will expire at
    const expiresAt = (authResult.expiresIn * 1000) + Date.now();

    this.accessToken = authResult.accessToken;
    this.idToken = authResult.idToken;
    this.expiresAt = expiresAt;
  }

  public renewTokens(): void {

    this.checkSession()
      .then( (authResult: auth0.Auth0DecodedHash) => {
        this.localLogin(authResult);
      })
      .catch( (err: auth0.Auth0Error) => {
        alert(`Could not get a new token (${err.error}: ${err.error_description}).`);
        this.logout();
      });
  }

  private checkSession() {
    return new Promise( (resolve, reject) => {
      this.auth0.checkSession({}, (err: auth0.Auth0Error, authResult: auth0.Auth0DecodedHash) => {

        if (authResult && authResult.accessToken && authResult.idToken) {
          resolve(authResult);
        } else {
          reject(err);
        }

      });
    });
  }

  public logout(): Promise<void> {
    // Remove tokens and expiry time
    this._storage.remove(Auth0Service.AccessTokenStorageKey);
    this._storage.remove(Auth0Service.IdTokenStorageKey);
    this._storage.remove(Auth0Service.ExpiresAtTokenStorageKey);

    this.auth0.logout({ returnTo: 'http://localhost:4200/login' });
    return Promise.resolve();
  }

  public isAuthenticated(): boolean {
    // Check whether the current time is past the
    // access token's expiry time
    // console.log(this.accessToken);
    // console.log(Date.now() < this.expiresAt);

    return this.accessToken && Date.now() < this.expiresAt;
  }

  public getProfile(): Promise<auth0.Auth0UserProfile> {
    return new Promise<auth0.Auth0UserProfile>( (resolve, reject) => {

      if (!this.accessToken) {
        reject('Access Token must exist to fetch profile');
      }

      if (this.userProfile) {
        resolve(this.userProfile);
      }

      this.auth0.client.userInfo(this.accessToken, (err, profile) => {
        if (profile) {
          this.userProfile = profile;
          resolve(profile);
        } else { reject(err); }
      });

    });
  }
}
