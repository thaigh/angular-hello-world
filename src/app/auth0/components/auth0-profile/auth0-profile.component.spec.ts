import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Auth0ProfileComponent } from './auth0-profile.component';

describe('Auth0ProfileComponent', () => {
  let component: Auth0ProfileComponent;
  let fixture: ComponentFixture<Auth0ProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Auth0ProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Auth0ProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
