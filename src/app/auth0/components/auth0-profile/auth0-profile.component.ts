import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-auth0-profile',
  templateUrl: './auth0-profile.component.html',
  styleUrls: ['./auth0-profile.component.css']
})
export class Auth0ProfileComponent implements OnInit {

  // tslint:disable-next-line:no-input-rename
  @Input('profile') profile: auth0.Auth0UserProfile;

  constructor() { }

  ngOnInit() {
  }

}
