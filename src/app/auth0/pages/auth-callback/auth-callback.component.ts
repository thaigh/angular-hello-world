import { Auth0Service } from './../../services/auth0.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth-callback',
  templateUrl: './auth-callback.component.html',
  styleUrls: ['./auth-callback.component.css']
})
export class AuthCallbackComponent implements OnInit {

  constructor(
    private authService: Auth0Service,
    private _router: Router
  ) { }

  ngOnInit() {
    this.authService.handleAuthentication()
    .then(success => {
      if (success) {
        console.log('is logged in');
        this._router.navigate(['/profile']);
      } else {
        console.log('err on login');
        this._router.navigate(['/login']);
      }
    })
    .catch( (err: auth0.Auth0ParseHashError) => {
      this._router.navigate(['/login']);
      console.log(err);
    });
  }

}
