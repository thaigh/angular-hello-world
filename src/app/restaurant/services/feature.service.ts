import { AngularFirestore } from '@angular/fire/firestore';
import { FirebaseCollectionService } from './../../shared/services/firebase-collection.service';
import { Injectable } from '@angular/core';
import { Feature } from '../models/feature';

@Injectable()
export class FeatureService extends FirebaseCollectionService<Feature> {

  private static readonly CollectionName = '/features';

  constructor(
    db: AngularFirestore
  ) {
    super(FeatureService.CollectionName, db);
  }
}
