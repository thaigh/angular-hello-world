import { FirebaseCollectionService } from './../../shared/services/firebase-collection.service';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map, mergeMap } from 'rxjs/operators';
import { Cuisine } from '../models/cuisine';
import { Observable, from } from 'rxjs';

@Injectable()
export class CuisinesService extends FirebaseCollectionService<Cuisine> {

  private static readonly CollectionName = '/cuisines';

  constructor(
    db: AngularFirestore
  ) {
    super(CuisinesService.CollectionName, db);
  }

}
