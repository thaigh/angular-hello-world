import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { FirebaseCollectionService } from './../../shared/services/firebase-collection.service';
import { Restaurant } from '../models/restaurant';

@Injectable()
export class RestaurantService extends FirebaseCollectionService<Restaurant> {

  private static readonly CollectionName = '/restaurants';
  constructor(
    db: AngularFirestore
  ) {
    super(RestaurantService.CollectionName, db);
  }


}
