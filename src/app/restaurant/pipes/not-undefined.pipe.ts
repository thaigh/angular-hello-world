import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'notUndefined'
})
export class NotUndefinedPipe implements PipeTransform {

  transform(value: any[], args?: any): any {
    const returnedValues = [];

    if (!value) { return null; }

    value.forEach(v => {
      if (v) { returnedValues.push(v); }
    });
    return returnedValues;
  }

}
