import { Cuisine } from './cuisine';
import { FirebaseDocument } from './../../shared/services/firebase-collection.service';
import { Observable, Subject } from 'rxjs';
import { Feature } from './feature';

export interface Restaurant extends FirebaseDocument {
  name: string;

  address: Address;

  cuisine: string; // Reference to Cuisine Document ID
  cuisineType: Cuisine; // Resolved Cuisine Document

  features: string[]; // Reference to Features Document ID
  featureTypes: Feature[]; // Resolved Feature Object

  rating: number;
}

export interface Address {
  city: string;
}
