import { FirebaseDocument } from './../../shared/services/firebase-collection.service';

// tslint:disable-next-line:no-empty-interface
export interface Feature extends FirebaseDocument {
  name: string;
}
