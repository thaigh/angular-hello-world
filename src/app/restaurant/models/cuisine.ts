import { FirebaseDocument } from './../../shared/services/firebase-collection.service';
export interface Cuisine extends FirebaseDocument {
  name: string;
}
