import { FeatureService } from './services/feature.service';
import { RestaurantService } from './services/restaurant.service';
import { FormsModule } from '@angular/forms';
import { CuisinesService } from './services/cuisines.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RestaurantRoutingModule } from './restaurant-routing.module';
import { RestaurantListComponent } from './pages/restaurant-list/restaurant-list.component';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireModule } from '@angular/fire';
import { NotUndefinedPipe } from './pipes/not-undefined.pipe';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [RestaurantListComponent, NotUndefinedPipe],
  imports: [
    CommonModule,
    RestaurantRoutingModule,
    AngularFireModule,
    AngularFirestoreModule,
    FormsModule,
    SharedModule
  ],
  exports: [
    RestaurantListComponent
  ],
  providers: [
    CuisinesService,
    RestaurantService,
    FeatureService
  ]
})
export class RestaurantModule { }
