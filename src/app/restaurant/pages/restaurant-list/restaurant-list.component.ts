import { EchoPipe } from './../../../shared/pipes/echo.pipe';
import { Cuisine } from './../../models/cuisine';
import { Component, OnInit } from '@angular/core';
import { Observable, forkJoin, from, merge, combineLatest, of, pipe, concat } from 'rxjs';
import { CuisinesService } from '../../services/cuisines.service';
import { Restaurant } from '../../models/restaurant';
import { RestaurantService } from '../../services/restaurant.service';
import { map, mergeAll, concatAll, combineAll, filter, switchMap, mergeMap, concatMap, take } from 'rxjs/operators';
import { FeatureService } from '../../services/feature.service';
import { Feature } from '../../models/feature';
import { NotUndefinedPipe } from '../../pipes/not-undefined.pipe';

@Component({
  selector: 'app-restaurant-list',
  templateUrl: './restaurant-list.component.html',
  styleUrls: ['./restaurant-list.component.css']
})
export class RestaurantListComponent implements OnInit {

  title = 'Cuisines';
  cuisines$: Observable<Cuisine[]>;
  features$: Observable<Feature[]>;
  restaurants$: Observable<Restaurant[]>;

  cuisineName = '';

  constructor(
    private _cuisinesService: CuisinesService,
    private _restaurantService: RestaurantService,
    private _featureService: FeatureService
  ) { }

  ngOnInit() {
    this.cuisines$ = this._cuisinesService.getAll();
    this.features$ = this._featureService.getAll();
    this.restaurants$ = this._restaurantService.getAll(ref => ref.where('rating',  '>=', 3).orderBy('rating'))
      .pipe(concatMap(r => this.resolveRestaurantReferences(r)));

  }

  resolveRestaurantReferences(restaurants: Restaurant[]) {
    return from(restaurants)
      .pipe(
        map(r => this.getRestaurantReferenceData(r)),
        combineAll(),
      );
  }

  private getRestaurantReferenceData(r: Restaurant) {
    const cuisines = this._cuisinesService.get(r.cuisine);

    const features = from(r.features)
      .pipe(
        map(f => this._featureService.get(f)),
        combineAll()
      );

    return combineLatest([ of(r), cuisines, features ])
      .pipe(map(resMap => this.updateRestaurantModelWithResolvedData(resMap)));
  }

  private updateRestaurantModelWithResolvedData(res: [Restaurant, Cuisine, Feature[]]) {

    const restaurant = res[0];
    const cuisineType = res[1];
    const features = res[2];

    restaurant.cuisineType = cuisineType;
    restaurant.featureTypes = features;
    return restaurant;
  }

  addCuisine() {
    this._cuisinesService.add({ id: '', name: this.cuisineName });
  }


}
