import { FacebookGraphApiService } from './services/facebook-graph-api.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    FacebookGraphApiService
  ]
})
export class FacebookModule { }
