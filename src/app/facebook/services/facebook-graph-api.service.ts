import { HttpClient } from '@angular/common/http';
import { LocalStorageService } from './../../shared/services/local-storage.service';
import { Injectable } from '@angular/core';

@Injectable()
export class FacebookGraphApiService {

  public static readonly FacebookAccessTokenKey = 'facebookAccessToken';

  constructor(
    private _localStorage: LocalStorageService,
    private _http: HttpClient
  ) { }

  getUserProfile(userId: string) {
    const accessToken = this._localStorage.get(FacebookGraphApiService.FacebookAccessTokenKey);
    const url = `https://graph.facebook.com/${userId}?fields=id,name&access_token=${accessToken}`;
    return this._http.get(url);
  }
}
