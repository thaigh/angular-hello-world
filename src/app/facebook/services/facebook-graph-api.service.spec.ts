import { TestBed } from '@angular/core/testing';

import { FacebookGraphApiService } from './facebook-graph-api.service';

describe('FacebookGraphApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FacebookGraphApiService = TestBed.get(FacebookGraphApiService);
    expect(service).toBeTruthy();
  });
});
