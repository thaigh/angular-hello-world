import { Component, OnInit } from '@angular/core';
import { CoursesService } from '../../services/courses.service';
import { AsyncPipe } from '@angular/common';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.css']
})
export class CourseListComponent implements OnInit {

  title = 'List of Courses';
  // courses: string[];
  coursesObservable$: Observable<string[]>;
  active = true;
  email = 'example@me.com';
  longText = 'Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing';

  constructor(private _coursesService: CoursesService ) { }

  ngOnInit() {
    this.coursesObservable$ = this._coursesService.getCourses();
    // this.coursesObservable$.subscribe(c => {
    //     this.courses = c;
    //   });
  }

  getTitle() { return this.title; }

  onSave($event) {
    console.log('Button was clicked', $event);
    $event.stopPropagation();
  }

  onKeyUp() {
    console.log('key up', this.email);
  }
}
