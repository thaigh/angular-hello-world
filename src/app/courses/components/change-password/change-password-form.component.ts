import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { PasswordValidator } from './validators/password-validator';

@Component({
  selector: 'app-change-password-form',
  templateUrl: './change-password-form.component.html',
  styleUrls: ['./change-password-form.component.css']
})
export class ChangePasswordFormComponent implements OnInit {

  form = new FormGroup({
    oldPassword: new FormControl('', [Validators.required], PasswordValidator.invalidOldPassword),
    newPassword: new FormControl('', [Validators.required, Validators.minLength(3)]),
    confirmPassword: new FormControl('', [Validators.required, Validators.minLength(3)])
  }, {
    validators: PasswordValidator.passwordsInFormShouldMatch
  });

  constructor() { }

  ngOnInit() {
  }

  get oldPassword() { return this.form.get('oldPassword'); }
  get newPassword() { return this.form.get('newPassword'); }
  get confirmPassword() { return this.form.get('confirmPassword'); }

  changePassword() {
    // if (this.newPassword.value !== this.confirmPassword.value) {
    //   this.form.setErrors({newPasswordMismatch: true});
    // }

  }

}
