import { AbstractControl, ValidationErrors } from '@angular/forms';
import { Observable } from 'rxjs';

export class PasswordValidator {

  static invalidOldPassword(control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {

    return new Promise( (resolve) => {
      setTimeout( () => {
        if (control.value !== '123') {
          resolve({ invalidOldPassword: true });
        } else {
          resolve(null);
        }
      }, 1000);
    });
  }

  static passwordsInFormShouldMatch(control: AbstractControl) {
    const newPassword = control.get('newPassword');
    const confirmPassword = control.get('confirmPassword');

    return PasswordValidator.passwordsShouldMatch(newPassword.value, confirmPassword.value);
  }

  // static passwordControlValuesShouldMatch(newPassword: AbstractControl, confirmPassword: AbstractControl) {
  //   return this.passwordsShouldMatch(newPassword.value, confirmPassword.value);
  // }

  static passwordsShouldMatch(newPassword: string, confirmPassword: string) {
    if (newPassword !== confirmPassword) {
      return {passwordsShouldMatch: true};
    }

    return null;
  }
}
