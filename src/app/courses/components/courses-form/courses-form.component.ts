import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-courses-form',
  templateUrl: './courses-form.component.html',
  styleUrls: ['./courses-form.component.css']
})
export class CoursesFormComponent implements OnInit {

  courseCategories = [ {id: 1, name: 'Development' }, { id: 2, name: 'Business' } ];

  constructor() { }

  ngOnInit() {
  }

  onSubmit(form) {
    console.info(form.value);
  }

}
