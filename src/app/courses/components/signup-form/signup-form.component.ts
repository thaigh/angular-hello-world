import { SignupService } from './../../services/signup.service';
import { UsernameValidators } from './validators/username.validators';
import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.css']
})
export class SignupFormComponent {

  form = new FormGroup({
    account: new FormGroup({
      username: new FormControl(
        '',
        [Validators.required, Validators.minLength(3), UsernameValidators.cannotContainSpace],
        UsernameValidators.shouldBeUnique
      ),
      password: new FormControl('', Validators.required),
    })
  });

  constructor(private _signUpService: SignupService) {

  }

  get username() { return this.form.get('account.username'); }
  get password() { return this.form.get('account.password'); }

  signUp(formValue) {
    console.info(formValue);
    const isValid = this._signUpService.signup(formValue);
    if (!isValid) {
      this.form.setErrors({
        invalidSignUp: true
      });
    }
  }
}
