import { AbstractControl, ValidationErrors } from '@angular/forms';
import { Observable, from } from 'rxjs';

export class UsernameValidators {

  static cannotContainSpace(control: AbstractControl): ValidationErrors | null {
    if ((control.value as string).indexOf(' ') > 0) {
      const errors: ValidationErrors = { cannotContainSpace: true };
      return errors;
    }

    // Validation successful. Return no errors
    return null;
  }

  static shouldBeUnique(control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {

    return new Promise( (resolve, reject) => {
      setTimeout( () => {

        if (control.value === 'mosh') {
          resolve({ shouldBeUnique: true });
        } else {
          // Validation successful. Return no eros
          resolve(null);
        }
      }, 2000);

    });

  }
}
