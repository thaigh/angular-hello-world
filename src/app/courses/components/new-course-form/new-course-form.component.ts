import { Component, OnInit } from '@angular/core';
import { FormArray, FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-new-course-form',
  templateUrl: './new-course-form.component.html',
  styleUrls: ['./new-course-form.component.css']
})
export class NewCourseFormComponent implements OnInit {

  form = new FormGroup({
    name: new FormControl(),
    contact: new FormGroup({
      email: new FormControl(),
      phone: new FormControl()
    }),
    topics: new FormArray([])
  });

  constructor(private fb: FormBuilder) {
    // this.form = fb.group({
    //   name: [ '', Validators.required ],
    //   contact: fb.group({
    //     email: [],
    //     phoner: []
    //   }),
    //   topics: fb.array([])
    // });
  }

  ngOnInit() {
  }

  get topics(): FormArray { return this.form.get('topics') as FormArray; }

  addTopic(topic: HTMLInputElement) {
    this.topics.push(new FormControl(topic.value));
    topic.value = '';
  }

  removeTopic(topic: FormControl) {
    const idx = this.topics.controls.indexOf(topic);
    this.topics.removeAt(idx);
  }

}
