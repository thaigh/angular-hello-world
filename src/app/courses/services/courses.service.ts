import { Injectable } from '@angular/core';
import { of, Observable } from 'rxjs';
import { delay } from 'rxjs/operators';

@Injectable()
export class CoursesService {

  constructor() { }

  getCourses(): Observable<string[]> {
    const courses = [ 'Course1', 'Course2', 'Course3' ];
    const observable = of(courses).pipe(delay(1000));
    return observable;
  }

}
