import { SharedModule } from './../shared/shared.module';
import { CourseComponent } from './components/course/course.component';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CourseListComponent } from './components/course-list/course-list.component';
import { CoursesService } from './services/courses.service';
import { CoursesFormComponent } from './components/courses-form/courses-form.component';
import { SignupFormComponent } from './components/signup-form/signup-form.component';
import { NewCourseFormComponent } from './components/new-course-form/new-course-form.component';
import { ChangePasswordFormComponent } from './components/change-password/change-password-form.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    ReactiveFormsModule
  ],
  declarations: [
    CourseComponent,
    CourseListComponent,
    CoursesFormComponent,
    SignupFormComponent,
    NewCourseFormComponent,
    ChangePasswordFormComponent
  ],
  exports: [
    CourseComponent,
    CourseListComponent,
    CoursesFormComponent,
    SignupFormComponent,
    NewCourseFormComponent,
    ChangePasswordFormComponent
  ],
  providers: [CoursesService]
})
export class CoursesModule { }
