import { ContactMethodService, IContactMethod } from './../../services/contact-method.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.css']
})
export class ContactFormComponent implements OnInit {

  contactMethods$: Observable<IContactMethod[]>;
  // methods: IContactMethod[];

  constructor(private _contactMethodService: ContactMethodService) { }

  ngOnInit() {
    this.contactMethods$ = this._contactMethodService.getContactMethods();
    // this.contactMethods$.subscribe(methods => this.methods = methods);
  }

  log(x) { console.info(x); }

  submit(form) {
    console.info(form);
  }

}
