import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ContactMethodService {

  constructor() { }

  getContactMethods(): Observable<IContactMethod[]> {
    const methods = [
      {id: 1, name: 'Email'},
      {id: 2, name: 'Phone'},
    ];
    const observable = of(methods).pipe(delay(1000));
    return observable;
  }
}


export interface IContactMethod {
  id: number;
  name: string;
}
