import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactFormComponent } from './components/contact-form/contact-form.component';

@NgModule({
  declarations: [ContactFormComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    ContactFormComponent
  ]
})
export class ContactsModule { }
