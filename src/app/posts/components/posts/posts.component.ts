import { BadInputError } from './../../models/bad-input-error';
import { PostsService } from './../../services/posts.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { IPost } from '../../models/post';
import { Error } from '../../models/error';
import { NotFoundError } from '../../models/not-found-error';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  posts$: Observable<IPost[]>;
  posts: IPost[];

  constructor(private _postsService: PostsService) {
  }

  ngOnInit() {
    this.posts$ = this._postsService.getPosts();
    this.posts$.subscribe(
      posts => this.posts = posts,
      err => {
        console.log('Unexpected error has occurred');
      });
  }

  createPost(input: HTMLInputElement) {
    const post: IPost = {title: input.value};
    input.value = '';

    this._postsService.createPost(post).subscribe(
      response => {
        console.log(response);
        post.id = response.id;
        this.posts.splice(0, 0, post);
      },
      (err: Error) => {
        if (err instanceof BadInputError) {
          console.log('Bad Input', err);
        } else {
          console.log('Unexpected error has occurred');
        }
      }
    );
  }

  updatePost(post: IPost) {
    this._postsService.patchPost({ ...post, isRead: true })
      .subscribe(
        resp => console.log(resp),
        err => {
          console.log('Unexpected error has occurred');
        });
  }

  deletePost(post: IPost) {
    this._postsService.deletePost(post)
      .subscribe(
        resp => {
          console.log(resp);
          const idx = this.posts.indexOf(post);
          this.posts.splice(idx, 1);
        },
        (err: Error) => {
          if (err instanceof NotFoundError) {
            console.log('Post has already been deleted');
          } else {
            console.log('Unexpected error has occurred');
          }
        }
      );
  }

}
