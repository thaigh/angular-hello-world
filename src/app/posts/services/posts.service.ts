import { Injectable } from '@angular/core';
import { IPost } from '../models/post';
import { map, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { Error } from '../models/error';
import { NotFoundError } from '../models/not-found-error';
import { BadInputError } from '../models/bad-input-error';

@Injectable()
export class PostsService {

  baseUrl = 'https://jsonplaceholder.typicode.com/posts';

  constructor(private _http: HttpClient) { }

  getPosts() {
    return this._http.get<IPost[]>(`${this.baseUrl}`);
  }

  createPost(post: IPost) {
    return this._http.post<IPost>(`${this.baseUrl}`, post)
      .pipe(catchError( (err: Response) => {
        if (err.status === 400) {
          return throwError(new BadInputError(err));
        } else {
          return throwError(new Error(err));
        }
      }));
  }

  putPost(post: IPost) {
    return this._http.put<IPost>(`${this.baseUrl}/${post.id}`, post);
  }

  patchPost(post: IPost) {
    return this._http.patch<IPost>(`${this.baseUrl}/${post.id}`, post);
  }

  deletePost(post: IPost) {
    return this._http.delete<IPost>(`${this.baseUrl}/${post.id}`)
      .pipe(catchError( (err: Response) => {
        if (err.status === 404) {
          return throwError(new NotFoundError({}));
        } else {
          return throwError(new Error(err));
        }
      }));
  }
}
