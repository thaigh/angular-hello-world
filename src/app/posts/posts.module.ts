import { PostsService } from './services/posts.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostsComponent } from './components/posts/posts.component';
import { HttpClientModule } from '@angular/common/http';
import { PostsRoutingModule } from './posts-routing.module';


@NgModule({
  declarations: [PostsComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    PostsRoutingModule
  ],
  exports: [
    PostsComponent
  ],
  providers: [ PostsService ]
})
export class PostsModule { }
