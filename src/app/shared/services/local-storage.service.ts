import { Injectable } from '@angular/core';

@Injectable()
export class LocalStorageService {

  constructor() { }

  get(key: string): string | undefined {
    if (localStorage.getItem(key)) {
      return localStorage.getItem(key);
    } else {
      return undefined;
    }
  }

  getData<T>(key: string): T | undefined {
    const data = this.get(key);
    if (data) {
      return JSON.parse(data) as T;
    } else {
      return undefined;
    }
  }

  set(key: string, data: string) {
    return localStorage.setItem(key, data);
  }

  setData(key: string, data: any) {
    const json = JSON.stringify(data, undefined, 2);
    return this.set(key, json);
  }

  remove(key: string) {
    localStorage.removeItem(key);
  }

  clear() {
    localStorage.clear();
  }
}
