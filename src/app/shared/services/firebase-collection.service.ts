import { Injectable } from '@angular/core';
import { AngularFirestore, QueryFn } from '@angular/fire/firestore';
import { from, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class FirebaseCollectionService<T extends FirebaseDocument> {

  constructor(
    private collectionName: string,
    private db: AngularFirestore
  ) { }

  getAll(queryRef?: QueryFn) {
    return this.db.collection<T>(this.collectionName, queryRef).valueChanges();
  }

  get(id: string) {
    return from(
      this.db.collection<T>(this.collectionName)
      .doc(id)
      .get()
    ).pipe(map( (data) => <T>data.data()));
  }

  add(data: T) {
    data.id = this.db.createId();
    return from(
        this.db.collection<T>(this.collectionName)
        .doc(data.id)
        .set(data)
    ).pipe(() => of(data));
  }

  update(data: T) {
    return from(
      this.db.collection<T>(this.collectionName)
        .doc(data.id)
        .set(data)
    ).pipe( () => of(data));
  }

  delete(data: T) {
    return of(
      this.db.collection<T>(this.collectionName)
      .doc(data.id)
      .delete()
    ).pipe( () => of(data));
  }
}

export interface FirebaseDocument {
  id: string;
}
