import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-like',
  templateUrl: './like.component.html',
  styleUrls: ['./like.component.css']
})
export class LikeComponent implements OnInit {

  constructor(
    private _likesCount: number,
    private _isSelected: boolean
  ) { }

  public get likesCount() { return this._likesCount; }
  public get isSelected() { return this._isSelected; }

  onClick() {
    this._likesCount += this._isSelected ? -1 : 1;
    this._isSelected = !this._isSelected;
  }

  ngOnInit() {
  }

}
