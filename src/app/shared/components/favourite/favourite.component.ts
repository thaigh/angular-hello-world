import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-favourite',
  templateUrl: './favourite.component.html',
  styleUrls: ['./favourite.component.css']
})
export class FavouriteComponent implements OnInit {

  // tslint:disable-next-line:no-input-rename
  @Input('isFavourite') isFavourite: boolean;

  // tslint:disable-next-line:no-output-rename
  @Output('change') change = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  onClick() {
    this.isFavourite = !this.isFavourite;
    this.change.emit({ newValue: this.isFavourite });
  }

}



export interface FavouriteChangedEventrArgs {
  newValue: boolean;
}
