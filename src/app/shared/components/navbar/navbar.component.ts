import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  // tslint:disable-next-line:no-input-rename
  @Input('routes') routes: NavigationRoute[];

  // tslint:disable-next-line:no-input-rename
  @Input('defaultRoute') defaultRoute: NavigationRoute = undefined;

  private activeRoute: NavigationRoute;

  constructor() { }

  ngOnInit() {
    this.activeRoute = this.defaultRoute;
  }

  routeIsActive(route: NavigationRoute) {
    return route.url === this.activeRoute.url;
  }

}

export interface NavigationRoute {
  url: string;
  displayName: string;
  queryParams?: { [key: string]: any };
}
