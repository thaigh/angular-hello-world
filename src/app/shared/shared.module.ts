import { LocalStorageService } from './services/local-storage.service';
import { FirebaseCollectionService } from './services/firebase-collection.service';
import { RouterModule } from '@angular/router';
import { LikeComponent } from './components/like/like.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SummaryPipe } from './pipes/summary.pipe';
import { FavouriteComponent, FavouriteChangedEventrArgs } from './components/favourite/favourite.component';
import { PanelComponent } from './components/panel/panel.component';
import { PhoneFormatDirective } from './directives/phone-format.directive';
import { NavbarComponent } from './components/navbar/navbar.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { EchoPipe } from './pipes/echo.pipe';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [
    LikeComponent,
    SummaryPipe,
    FavouriteComponent,
    PanelComponent,
    PhoneFormatDirective,
    NavbarComponent,
    NotFoundComponent,
    EchoPipe
  ],
  exports: [
    LikeComponent,
    SummaryPipe,
    FavouriteComponent,
    PanelComponent,
    PhoneFormatDirective,
    NavbarComponent,
    NotFoundComponent,
    EchoPipe
  ],
  providers: [
    FirebaseCollectionService,
    LocalStorageService
  ]
})
export class SharedModule { }
