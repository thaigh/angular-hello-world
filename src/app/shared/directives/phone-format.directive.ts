import { Directive, HostListener, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[appPhoneFormat]'
})
export class PhoneFormatDirective {

  constructor(private el: ElementRef) { }

  // @HostListener('focus') onFocus() {
  //   console.info('on focus');
  // }

  // tslint:disable-next-line:no-input-rename
  @Input('appPhoneFormat') format: string;

  @HostListener('blur') onBlur() {
    const value: string = this.el.nativeElement.value;
    this.el.nativeElement.value = this.format === 'lowercase'
      ? value.toLowerCase()
      : value.toUpperCase();
  }

}
