import { Auth0Service } from './../../auth0/services/auth0.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { Injectable } from '@angular/core';
import { auth } from 'firebase/app';
import { map, mergeMap, take, first, takeLast, delay } from 'rxjs/operators';
import { of, from, merge, forkJoin, zip, EMPTY, Observable } from 'rxjs';

@Injectable()
export class AuthService {



  constructor(
    // private af: AngularFire
    private afAuth: AngularFireAuth,
    private auth0Service: Auth0Service
  ) { }


  loginWithFacebook() {
    return this.afAuth.auth.signInWithPopup(new auth.FacebookAuthProvider());
  }

  logout() {
    const afLogout = from(this.afAuth.auth.signOut());

    const auth0Logout = this.auth0Service.isAuthenticated()
      ? from(this.auth0Service.logout())
      : of(null);

    return forkJoin([afLogout, auth0Logout])
      .pipe(map( ([af, auth0]) => af))
      .pipe(take(1));
  }

  authState() {
    return this.afAuth.authState;
  }

  register(email: string, password: string) {
    return this.afAuth.auth.createUserWithEmailAndPassword(email, password);
  }

  loginWithEmailAndPassword(email: string, password: string) {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password);
  }

  isLoggedIn() {

    // See here for why you need to add take(1)
    // https://github.com/angular/angular/issues/9613
    // The observable needs to be completed for the route guard to work

    // BUG: The observable returns true, but the route guard interprets it as false
    // or perhaps, it takes too long to respond, so it assumes false.
    // But only happens on the first call.
    const afLogin = this.afAuth.authState
      .pipe(mergeMap( user => user ? of(true) : of(false)))
      .pipe(take(1));

    const auth0Login = of(this.auth0Service.isAuthenticated())
      .pipe(take(1));

    return forkJoin([afLogin, auth0Login])
      .pipe(map( ([af, auth0]) => af || auth0))
      .pipe(take(1));
  }

  isNotLoggedIn() {
    return this.isLoggedIn()
      .pipe( mergeMap( b => of(!b) ));
  }



}
