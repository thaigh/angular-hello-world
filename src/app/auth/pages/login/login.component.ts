import { Auth0Service } from './../../../auth0/services/auth0.service';
import { FacebookGraphApiService } from '../../../facebook/services/facebook-graph-api.service';
import { FacebookAuthCredential } from '../../models/facebook-auth-credential';
import { LocalStorageService } from '../../../shared/services/local-storage.service';
import { AuthService } from '../../services/auth.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable, Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private _authService: AuthService,
    private _localStorage: LocalStorageService,
    private _router: Router,
    private _auth0Service: Auth0Service
  ) { }

  ngOnInit() { }

  loginWithFacebook() {
    this._authService.loginWithFacebook()
      .then(authState => {
        console.log('USER LOGGED IN', authState);
        const credential = authState.credential as FacebookAuthCredential;
        const accessToken = credential.accessToken;
        this._localStorage.set(FacebookGraphApiService.FacebookAccessTokenKey, accessToken);

        this._router.navigate(['/profile']);
      });
  }


  loginWithUsernameAndPassword(formData: { email: string, password: string }) {
    this._authService.loginWithEmailAndPassword(formData.email, formData.password)
      .then( authState => {
        console.log('SUCCESS LOGIN USER', authState);
        this._router.navigate(['/profile']);
      });
  }


  loginWithAuth0() {
    this._auth0Service.login();
  }



  register() {
    this._router.navigate(['/register']);
  }

}
