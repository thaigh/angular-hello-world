import { Auth0Service } from './../../../auth0/services/auth0.service';
import { FacebookGraphApiService } from './../../../facebook/services/facebook-graph-api.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Observable, Subscribable, Subscription, from } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit, OnDestroy {

  authState$: Observable<firebase.User>;
  authStateSubscription$: Subscription;
  userProfile$: Observable<any>;
  auth0UserProfile$: Observable<any>;

  constructor(
    private _authService: AuthService,
    private _facebookGraphApi: FacebookGraphApiService,
    private _router: Router,
    private auth0Service: Auth0Service
  ) {
    this.getUserProfile();
  }

  ngOnInit() {
    // Not called when nagivated to from angular router
  }

  getUserProfile() {
    this.authState$ = this._authService.authState();

    this.authStateSubscription$ = this.authState$.subscribe(authState => {
      if (authState) {
        console.log('auth state', authState);
        this.userProfile$ = this._facebookGraphApi.getUserProfile(authState.providerData[0].uid);
      }
    });

    if (this.auth0Service.isAuthenticated()) {
      this.auth0UserProfile$ = from(this.auth0Service.getProfile());
    }


  }

  ngOnDestroy() {
    if (this.authStateSubscription$) {
      this.authStateSubscription$.unsubscribe();
    }
  }

  logout() {
    this._authService.logout()
      .subscribe( () => {
        this._router.navigate(['/login']);
      });
  }

}
