import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-profile-image',
  templateUrl: './profile-image.component.html',
  styleUrls: ['./profile-image.component.css']
})
export class ProfileImageComponent implements OnInit {

  // tslint:disable-next-line:no-input-rename
  @Input('user') user: firebase.User;

  // tslint:disable-next-line:no-input-rename
  @Input('height') height = 200;

  constructor() { }

  ngOnInit() {
  }

  get photoUrl() {
  // To get user's photoUrl: https://stackoverflow.com/a/49996107/2442468
  // this.afAuth.auth.currentUser.providerData[0].photoURL
    return this.user.providerData[0].photoURL;
  }

}
