import { Auth0Module } from './../auth0/auth0.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from './services/auth.service';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './pages/login/login.component';
import { NgxJsonViewerModule } from 'ngx-json-viewer';
import { ProfileImageComponent } from './components/profile-image/profile-image.component';
import { RegisterFormComponent } from './forms/register-form/register-form.component';
import { LoginFormComponent } from './forms/login-form/login-form.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { RegisterComponent } from './pages/register/register.component';
import { NotLoggedInGuard } from './guards/not-logged-in.guard';
import { LoggedInGuard } from './guards/logged-in.guard';

@NgModule({
  declarations: [LoginComponent, ProfileImageComponent, RegisterFormComponent, LoginFormComponent, ProfileComponent, RegisterComponent],
  imports: [
    CommonModule,
    AuthRoutingModule,
    AngularFireAuthModule,
    NgxJsonViewerModule,
    FormsModule,
    ReactiveFormsModule,
    Auth0Module
  ],
  exports: [
    LoginComponent
  ],
  providers: [
    AuthService,
    NotLoggedInGuard,
    LoggedInGuard
  ]
})
export class AuthModule { }
