import * as firebase from 'firebase';

export class FacebookAuthCredential extends firebase.auth.AuthCredential {
  accessToken: string;
}
