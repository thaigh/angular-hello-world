import { AuthService } from '../../services/auth.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit {

  form: FormGroup;
  authError: any;

  constructor(
    private fb: FormBuilder,
    private _authService: AuthService,
    private _router: Router
  ) { }

  ngOnInit() {
    this.form = this.fb.group({
      email: this.fb.control(''),
      password: this.fb.control(''),
    });
  }

  private get email() { return this.form.get('email').value; }
  private get password() { return this.form.get('password').value; }

  register() {
    this.authError = null;

    this._authService.register(this.email, this.password)
      .then( authState => {
        console.log('SUCCESS REGISTER USER', authState);
        this._router.navigate(['/profile']);
      })
      .catch( err => {
        console.log('FAIL REGISTER USER', err);
        this.authError = err;
      });
  }

}
