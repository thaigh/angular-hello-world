import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  // tslint:disable-next-line:no-output-rename
  @Output('login') login = new EventEmitter();

  form: FormGroup;

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.form = this.fb.group({
      email: this.fb.control(''),
      password: this.fb.control(''),
    });
  }

  private get email() { return this.form.get('email').value; }
  private get password() { return this.form.get('password').value; }

  submitLogin() {
    this.login.emit({email: this.email, password: this.password});
  }

}
