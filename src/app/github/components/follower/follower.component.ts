import { Follower } from './../../models/follower';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-follower',
  templateUrl: './follower.component.html',
  styleUrls: ['./follower.component.css']
})
export class FollowerComponent implements OnInit {

  // tslint:disable-next-line:no-input-rename
  @Input('follower') follower: Follower;

  constructor() { }

  ngOnInit() {

  }

}
