import { ActivatedRoute } from '@angular/router';
import { FollowersService } from './../../services/followers.service';
import { Component, OnInit } from '@angular/core';
import { Observable, combineLatest, of } from 'rxjs';
import { Follower } from '../../models/follower';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-followers',
  templateUrl: './followers.component.html',
  styleUrls: ['./followers.component.css']
})
export class FollowersComponent implements OnInit {

  followers$: Observable<Follower[]>;

  constructor(
    private _followersService: FollowersService,
    private _route: ActivatedRoute
  ) { }

  ngOnInit() {

    combineLatest([
      this._route.paramMap,
      this._route.queryParamMap,
    ])
    .pipe(map(combined => {
      const params = {
        page: combined[1].get('page'),
        order: combined[1].get('newest')
      };

      return of(params);
    }))
    .subscribe(params => {
      this.followers$ = this._followersService.getFollowers('tylerhaigh');
    });

  }

}
