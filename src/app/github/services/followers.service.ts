import { Follower } from './../models/follower';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FollowersService {

  constructor(private _httpClient: HttpClient) { }

  getFollowers(user: string) {
    const followersApiUrl = `https://api.github.com/users/${user}/followers`;
    return this._httpClient.get<Follower[]>(followersApiUrl);
  }
}
