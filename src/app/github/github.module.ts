import { FollowersService } from './services/followers.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FollowersComponent } from './components/followers/followers.component';
import { FollowerComponent } from './components/follower/follower.component';
import { ProfileComponent } from './components/profile/profile.component';
import { GithubRoutingModule } from './github-routing.module';

@NgModule({
  declarations: [FollowersComponent, FollowerComponent, ProfileComponent],
  imports: [
    CommonModule,
    GithubRoutingModule
  ],
  exports: [
    FollowersComponent,
    FollowerComponent
  ],
  providers: [
  ]
})
export class GithubModule { }
