import { BlogArchiveComponent } from './pages/blog-archive/blog-archive.component';
import { BlogHomeComponent } from './pages/blog-home/blog-home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'blogs/:year/:month', component: BlogArchiveComponent },
  { path: 'blogs', component: BlogHomeComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BlogRoutingModule { }
