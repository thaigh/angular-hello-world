import { BlogService, BlogArchive } from './../../services/blog.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-blog-home',
  templateUrl: './blog-home.component.html',
  styleUrls: ['./blog-home.component.css']
})
export class BlogHomeComponent implements OnInit {

  blogArchiveList$: Observable<BlogArchive[]>;

  constructor(
    private _blogService: BlogService
  ) { }

  ngOnInit() {
    this.blogArchiveList$ = this._blogService.getBlogArchiveList();
  }

}
