import { Injectable } from '@angular/core';
import { of, from } from 'rxjs';
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  constructor() { }

  getBlogArchiveList() {
    const blogArchives: BlogArchive[] = [
      { year: 2017, month: 1 },
      { year: 2017, month: 2 },
      { year: 2017, month: 3 }
    ];

    return of(blogArchives).pipe(delay(500));
  }
}

export interface BlogArchive {
  year: number;
  month: number;
}
