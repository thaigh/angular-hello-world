import { BlogService } from './services/blog.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BlogRoutingModule } from './blog-routing.module';
import { BlogHomeComponent } from './pages/blog-home/blog-home.component';
import { RouterModule } from '@angular/router';
import { BlogArchiveComponent } from './pages/blog-archive/blog-archive.component';

@NgModule({
  declarations: [BlogHomeComponent, BlogArchiveComponent],
  imports: [
    CommonModule,
    BlogRoutingModule,
    RouterModule
  ],
  exports: [
    BlogHomeComponent
  ],
  providers: [
    BlogService
  ]
})
export class BlogModule { }
